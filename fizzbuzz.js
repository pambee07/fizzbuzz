// Affiche Fizzbuzz
console.log("FizzBuzz");

// Boucle de 100 fois
for (let num = 0; num <= 100; num++) {
  if (num % 3 === 0 && num % 5 === 0 /*si c'est multiple de 3 et 5*/) {
    // Affiche Fizzbuzz
    console.log("FizzBuzz");
  } else {
    if (num % 3 === 0 /*si c'est multiple de 3*/) {
      // Affiche Fizz
      console.log("Fizz");
    } else {
      if (num % 5 === 0 /*si c'est multiple de 5*/) {
        // Affiche buzz
        console.log("Buzz");
      } else {
        // j'affiche num
        console.log(num);
      }
    }
  }
}
